#!/usr/bin/env python
import sys


from typing import Any


def add_any(x: Any, y: Any):
    return x + y


def add_object(x: object, y: object):
    return x + y


def main():
    # noinspection PyUnusedLocal
    args = sys.argv


if __name__ == '__main__':
    main()
