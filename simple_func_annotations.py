#!/usr/bin/env python
import sys


def zero_fill(n: int, digits: int) -> str:
    return '{{:0{:d}d}}'.format(digits).format(n)


def test_zero_fill():
    assert zero_fill(3, 3) == '003'
    assert zero_fill(12, 4) == '0012'
    assert zero_fill(321, 2) == '321'


def main():
    zero_fill(3, 3)
    zero_fill(3.0, 3)


if __name__ == '__main__':
    main()
