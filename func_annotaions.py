#!/usr/bin/env python
import io
import json
import sys
from typing import List, Tuple, Union

DATA = """[[1000, "札幌市", ["中央区", "北区", "東区", "白石区", "豊平区", "南区", "西区", "厚別区",  """ \
       """"手稲区", "清田区"]]\n[4100, "仙台市", ["青葉区", "宮城野区", "若林区", "太白区", "泉区"]]]"""


def parse_data(target_io: Union[io.TextIOBase, io.TextIOWrapper]) -> List[Tuple[int, str, List[str]]]:
    data = []
    for i, l in enumerate(target_io):
        t = json.loads(l.rstrip('\n'))

        data.append((i, t.get('IntType', 0), t.get('StrType', ''), t.get('ArrayType', [])))

    return data


def main():
    data = parse_data(io.StringIO(DATA))

    for city in data:
        city[0]
        city[1]
        city[2]
        for ward in city[2]:
            ward

    data = parse_data(sys.stdin)
    with open('hoge') as f:
        data = parse_data(f)


if __name__ == '__main__':
    main()
