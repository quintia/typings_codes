#!/usr/bin/env python

from __future__ import annotations


class A:
    pass


class B:
    def f1(self) -> A:
        pass

    def f2(self) -> C:
        pass

class C:
    pass
