#!/usr/bin/env python

from typing import List, TypeVar

T = TypeVar('T')


def even_items(l: List[T]) -> List[T]:
    return [x for i, x in enumerate(l) if i % 2 == 0]


def main():
    int_list = even_items([1, 2, 3, 4, 5])  # => [1, 3, 5]
    str_list = even_items(['a', 'b', 'c'])  # => ['a', 'c']



if __name__ == '__main__':
    main()
