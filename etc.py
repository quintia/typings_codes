#!/usr/bin/env python

from typing import Iterable, TypeVar

T = TypeVar('T')


def repeat(item: T) -> Iterable[T]:
    while 1:
        yield item


def main():
    for s in repeat('a'):
        s


if __name__ == '__main__':
    main()
