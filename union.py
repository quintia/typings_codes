#!/usr/bin/env python

import math
from typing import List, Union


def norm(l: List[Union[int, float]]) -> float:
    return math.sqrt(sum(map(lambda x: x ** 2, l)))


def main():
    print(norm([1, 2, 3]))
    print(norm([1.0, 2.0, 3.0]))
    print(norm([1, 2.0, 3]))
    print(norm(['a', 'b']))


if __name__ == '__main__':
    main()