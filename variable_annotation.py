#!/usr/bin/env python
import json
from typing import List, Tuple

FOO: int = 1


class Container:
    def __init__(self) -> None:
        super().__init__()
        self.foo: str = ''
        self.bar: int = 0


DATA = """[[1000, "札幌市", ["中央区", "北区", "東区", "白石区", "豊平区", "南区", "西区", "厚別区",  """ \
       """"手稲区", "清田区"]],[4100, "仙台市", ["青葉区", "宮城野区", "若林区", "太白区", "泉区"]]]"""


def main():
    data: List[Tuple[int, str, List[str]]] = [tuple(l) for l in json.loads(DATA)]
    data[0][2]


if __name__ == '__main__':
    main()
