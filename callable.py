#!/usr/bin/env python
import sys
from typing import Callable


def my_map(func: Callable[[int], str], n: int):
    return func(n)


def main():
    my_map(lambda x: x ** 2, 5)


if __name__ == '__main__':
    main()
