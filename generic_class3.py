#!/usr/bin/env python
from typing import List, TypeVar, Generic, Any, Tuple

S = TypeVar('S')
T = TypeVar('T')
U = TypeVar('U')


class Token(Generic[T]):
    def __init__(self, val: T):
        self.val: T = val

    def __str__(self):
        return str(self.val)


StrToken = Token[str]


class PairToken(Token, Generic[S, U]):
    def __init__(self, val: Tuple[S, U]):
        super().__init__(val)
        self.first: S = val[0]
        self.second: U = val[1]

    def __str__(self):
        return '(' + str(self.first) + ', ' + str(self.second) + ')'


class SentencesToken(Token):
    def __init__(self, val: List[str]):
        super().__init__(val)

    def __getitem__(self, item) -> str:
        assert isinstance(item, int), 'item must be int, but given ' + type(item)
        return self.val[item]


def main():
    p = PairToken(('s', 1))
    x = p.first  # . でstrがサジェストされる
    x = p.second  # . でintがサジェストされる

    s = StrToken('aaa')
    x = s.val  # . サジェストされない

    t: StrToken = Token('aaa')
    x = t.val  # . でstrがサジェストされる

    u = SentencesToken(['a', 'b'])
    x = u[0]  # . でstrがサジェストされる

    # これが警告がでない(PyCharm, mypyとも)
    w = SentencesToken(['a', 1])
    x = w[1]  # str扱い


if __name__ == '__main__':
    main()
