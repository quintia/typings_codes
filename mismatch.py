#!/usr/bin/env python
import sys


def main():
    with open('foo', 'w') as f:
        print('なんとか', file=f)


if __name__ == '__main__':
    main()
