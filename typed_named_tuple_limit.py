#!/usr/bin/env python

from typing import List, NamedTuple

DATA = """[[1000, "札幌市", ["中央区", "北区", "東区", "白石区", "豊平区", "南区", "西区", "厚別区",  """ \
       """"手稲区", "清田区"]],[4100, "仙台市", ["青葉区", "宮城野区", "若林区", "太白区", "泉区"]]]"""


class Data(NamedTuple):
    code: int
    name: str
    wards: List[str]


def main():
    sendai = Data(4100, "仙台市", ['青葉区', '宮城野区', '若林区', '太白区', '泉区'])

    for ward in sendai.wards:
        address = sendai.name + ward + ...


if __name__ == '__main__':
    main()
