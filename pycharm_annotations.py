#!/usr/bin/env python

from typing import List


def q03(s: str) -> List[int]:
    return list(map(lambda x: len(list(filter(str.isalpha, x))), s.split()))


def main():
    pi = q03(
        'Now I need a drink, alcoholic of course, '
        'after the heavy lectures involving quantum mechanics.')
    # => [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9]


if __name__ == '__main__':
    main()
